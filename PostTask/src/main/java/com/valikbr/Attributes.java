package com.valikbr;

public enum Attributes {
    MIN_SENDER_INTERVAL("100"),
    MAX_SENDER_INTERVAL("1300"),
    POSTMAN_INTERVAL("1700"),
    POSTMAN_CARRYING_CAPACITY("2"),
    SENDER_LIFETIME("10000"),
    POSTMAN_LIFETIME("10000"),
    NUMBER_OF_SENDERS("1"),
    NUMBER_OF_POSTMANS("1"),
    ;
    private String attribute;

    public String getAttribute() {
        return attribute;
    }

    Attributes(String attribute) {
        this.attribute = attribute;
    }
}
