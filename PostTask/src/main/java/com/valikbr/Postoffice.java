package com.valikbr;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Postoffice {
    private final int capacity = 10;
    private BlockingQueue<Integer> queue = new ArrayBlockingQueue<>(capacity);

    public BlockingQueue<Integer> getQueue() {
        return queue;
    }

    public int getCapacity() {
        return capacity;
    }
}
