package com.valikbr;

import java.util.TimerTask;

public class InterruptTimerTask extends TimerTask {

    private Thread thread;

    public InterruptTimerTask(Thread thread) {
        this.thread = thread;
    }

    @Override
    public void run() {
        thread.interrupt();
    }
}
