package com.valikbr;

import java.util.Timer;
import java.util.concurrent.BlockingQueue;

public class Postman implements Runnable {
    private final BlockingQueue<Integer> queue;
    private int carryingCapacity;
    private long lifeTime;
    private static int parcelsGot = 0;

    public Postman(Postoffice postoffice, int carryingCapacity, long lifeTime) {
        this.queue = postoffice.getQueue();
        this.carryingCapacity = carryingCapacity;
        this.lifeTime = lifeTime;
    }

    @Override
    public void run() {
        Timer timer = new Timer(true);
        InterruptTimerTask interruptTimerTask = new InterruptTimerTask(Thread.currentThread());

        timer.schedule(interruptTimerTask, lifeTime);

        try {
            while (true) {
                for (int i = 0; i < carryingCapacity; i++) {

                    Integer take = queue.take();
                    parcelsGot++;
                    System.out.println("The parcel was taken " + take);
                }
                Thread.sleep(Integer.parseInt(Attributes.POSTMAN_INTERVAL.getAttribute()));
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public static int getParcelsGot() {
        return parcelsGot;
    }
}
