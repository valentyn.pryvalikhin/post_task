package com.valikbr;

public class App {
    public static void main(String[] args) {
        Postoffice postoffice = new Postoffice();

        ThreadsPool threadsPool = new ThreadsPool(
                Integer.parseInt(Attributes.NUMBER_OF_SENDERS.getAttribute()),
                Integer.parseInt(Attributes.NUMBER_OF_POSTMANS.getAttribute()),
                postoffice);

        try {
            threadsPool.InitThreadsPool();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Parcels at post: " + (postoffice.getCapacity() - postoffice.getQueue().remainingCapacity()));
        System.out.println("Parcels sent: " + Sender.getParcelsSent());
        System.out.println("Parcels got: " + Postman.getParcelsGot());

        if (Sender.getParcelsSent() == Postman.getParcelsGot() + (postoffice.getCapacity() - postoffice.getQueue().remainingCapacity())) {
            System.out.println("All right! All parcels in place");
        } else {
            System.out.println("Some parcels lost");
        }
    }
}
