package com.valikbr;

import java.util.Random;
import java.util.Timer;
import java.util.concurrent.BlockingQueue;

public class Sender implements Runnable {

    private final BlockingQueue<Integer> queue;
    private long lifeTime;
    private static int parcelsSent = 0;
    private int parcelNumber = 1;

    public Sender(Postoffice postoffice, long lifeTime) {
        this.queue = postoffice.getQueue();
        this.lifeTime = lifeTime;
    }

    @Override
    public void run() {
        Timer timer = new Timer(true);
        InterruptTimerTask interruptTimerTask = new InterruptTimerTask(Thread.currentThread());

        timer.schedule(interruptTimerTask, lifeTime);

        try {
            while (true) {
                putParcel();
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    private void putParcel() throws InterruptedException {

        queue.put(parcelNumber);
        System.out.println("The parcel was put " + parcelNumber);
        parcelNumber++;
        parcelsSent++;
        Thread.sleep(getRandomNumberInRange(
                Integer.parseInt(Attributes.MIN_SENDER_INTERVAL.getAttribute()),
                Integer.parseInt(Attributes.MAX_SENDER_INTERVAL.getAttribute())));
    }

    private int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("Max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    public static int getParcelsSent() {
        return parcelsSent;
    }
}
