package com.valikbr;

import java.util.ArrayList;
import java.util.List;

public class ThreadsPool {

    private List<Thread> senderThreadsList = new ArrayList<>();
    private List<Thread> postmanThreadsList = new ArrayList<>();
    private int senderThreadsQuantyty;
    private int postmanThreadsQuantyty;
    private Postoffice postoffice;

    public ThreadsPool(int senderThreadsQuantyty, int postmanThreadsQuantyty, Postoffice postoffice) {
        this.senderThreadsQuantyty = senderThreadsQuantyty;
        this.postmanThreadsQuantyty = postmanThreadsQuantyty;
        this.postoffice = postoffice;
    }

    public void InitThreadsPool() throws InterruptedException {
        for (int i = 0; i < senderThreadsQuantyty; i++) {
            senderThreadsList.add(new Thread(new Sender(postoffice,
                    Long.parseLong(Attributes.SENDER_LIFETIME.getAttribute()))));
        }

        for (int i = 0; i < postmanThreadsQuantyty; i++) {
            postmanThreadsList.add(new Thread(new Postman(postoffice,
                    Integer.parseInt(Attributes.POSTMAN_CARRYING_CAPACITY.getAttribute()),
                    Long.parseLong(Attributes.POSTMAN_LIFETIME.getAttribute()))));
        }

        for (Thread thread : senderThreadsList) {
            thread.start();
        }

        for (Thread thread : postmanThreadsList) {
            thread.start();
        }

        for (Thread thread : senderThreadsList) {
            thread.join();
        }

        for (Thread thread : postmanThreadsList) {
            thread.join();
        }
    }
}
